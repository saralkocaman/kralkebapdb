﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using kebapcidb.Models;

namespace kebapcidb.Controllers
{
    public class SiparisKayitController : Controller
    {
        private kebapcidbEntities db = new kebapcidbEntities();

        // GET: SiparisKayit
        public ActionResult Index()
        {
            var siparisKayit = db.SiparisKayit.Include(s => s.Musteri).Include(s => s.Siparis);
            return View(siparisKayit.ToList());
        }

        // GET: SiparisKayit/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SiparisKayit siparisKayit = db.SiparisKayit.Find(id);
            if (siparisKayit == null)
            {
                return HttpNotFound();
            }
            return View(siparisKayit);
        }

        // GET: SiparisKayit/Create
        public ActionResult Create()
        {
            ViewBag.MusteriID = new SelectList(db.Musteri, "MusteriID", "MusteriAd");
            ViewBag.SiparisID = new SelectList(db.Siparis, "SiparisID", "SiparisAd");
            return View();
        }

        // POST: SiparisKayit/Create
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KayitID,MusteriID,SiparisID,SiparisFiyat")] SiparisKayit siparisKayit)
        {
            if (ModelState.IsValid)
            {
                db.SiparisKayit.Add(siparisKayit);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MusteriID = new SelectList(db.Musteri, "MusteriID", "MusteriAd", siparisKayit.MusteriID);
            ViewBag.SiparisID = new SelectList(db.Siparis, "SiparisID", "SiparisAd", siparisKayit.SiparisID);
            return View(siparisKayit);
        }

        // GET: SiparisKayit/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SiparisKayit siparisKayit = db.SiparisKayit.Find(id);
            if (siparisKayit == null)
            {
                return HttpNotFound();
            }
            ViewBag.MusteriID = new SelectList(db.Musteri, "MusteriID", "MusteriAd", siparisKayit.MusteriID);
            ViewBag.SiparisID = new SelectList(db.Siparis, "SiparisID", "SiparisAd", siparisKayit.SiparisID);
            return View(siparisKayit);
        }

        // POST: SiparisKayit/Edit/5
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KayitID,MusteriID,SiparisID,SiparisFiyat")] SiparisKayit siparisKayit)
        {
            if (ModelState.IsValid)
            {
                db.Entry(siparisKayit).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MusteriID = new SelectList(db.Musteri, "MusteriID", "MusteriAd", siparisKayit.MusteriID);
            ViewBag.SiparisID = new SelectList(db.Siparis, "SiparisID", "SiparisAd", siparisKayit.SiparisID);
            return View(siparisKayit);
        }

        // GET: SiparisKayit/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SiparisKayit siparisKayit = db.SiparisKayit.Find(id);
            if (siparisKayit == null)
            {
                return HttpNotFound();
            }
            return View(siparisKayit);
        }

        // POST: SiparisKayit/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SiparisKayit siparisKayit = db.SiparisKayit.Find(id);
            db.SiparisKayit.Remove(siparisKayit);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
