﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(kebapcidb.Startup))]
namespace kebapcidb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
