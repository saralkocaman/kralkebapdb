﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace kebapcidb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Yedek()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Galeri()
        {
            return View();
        }
        public ActionResult Hakkımızda()
        {
            return View();
        }
        public ActionResult Menuler()
        {
            return View();
        }
        public ActionResult İletisim()
        {
            return View();
        }
        public ActionResult GirisYap()
        {
            return View();
        }
        public ActionResult Kaydol()
        {
            return View();
        }
        public ActionResult AdminGiris()
        {
            return View();
        }

    }
}